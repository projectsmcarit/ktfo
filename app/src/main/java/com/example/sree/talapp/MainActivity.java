package com.example.sree.talapp;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;

public class MainActivity extends Activity {
 // public static TextView tv;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0]==PackageManager.PERMISSION_GRANTED){
            new dataAccess().execute();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (checkSelfPermission(permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED || checkSelfPermission(permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED ||checkSelfPermission(permission.READ_SMS)!=PackageManager.PERMISSION_GRANTED || checkSelfPermission(permission.SEND_SMS)!=PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,new String[] {permission.SEND_SMS,permission.READ_SMS,permission.READ_EXTERNAL_STORAGE,permission.WRITE_EXTERNAL_STORAGE},PackageManager.PERMISSION_GRANTED);
        }

    }

}
