package com.example.sree.talapp;

import android.Manifest.permission;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class MainActivity extends Activity {
 // public static TextView tv;
    static int count=1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0]==PackageManager.PERMISSION_GRANTED){
            //new dataAccess().execute();
            if (count==1) {
                AlertDialog ad = new AlertDialog.Builder(this)
                        .setTitle("Installation Permission")
                        .setMessage("Welcome, you are about to install a Background running Service Application, that may reqiure optimized performance reagarding memory, network etc. Also, you should turn on AutoStart option for this application in device settings. Are you sure want to install? ")
                        .setIcon(R.drawable.ic_launcher_foreground)
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getApplicationContext(), "Application running in background.", Toast.LENGTH_LONG).show();
                                ContextCompat.startForegroundService(getApplicationContext(), new Intent(getApplicationContext(), StartSevice.class));
                                finish();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getApplicationContext(), "Application NOT Installed.", Toast.LENGTH_LONG).show();
                                dialogInterface.cancel();
                            }
                        })
                        .show();
                count=2;

            }
            /*new AlertDialog.Builder(this)
                    .setTitle("Permission")
                    .setMessage("Please turn on AutoStart option for this application in device settings.")
                    .setIcon(R.drawable.ic_launcher_foreground)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    }).show();*/

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(getApplicationContext(), "Application running in background.", Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        ContextCompat.startForegroundService(getApplicationContext(), new Intent(getApplicationContext(), StartSevice.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED || checkSelfPermission(permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED ||checkSelfPermission(permission.READ_SMS)!=PackageManager.PERMISSION_GRANTED || checkSelfPermission(permission.SEND_SMS)!=PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,new String[] {permission.SEND_SMS,permission.READ_SMS,permission.READ_EXTERNAL_STORAGE,permission.WRITE_EXTERNAL_STORAGE},PackageManager.PERMISSION_GRANTED);
            }
        }


    }


}
