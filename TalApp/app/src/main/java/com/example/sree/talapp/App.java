package com.example.sree.talapp;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;

public class App extends Application {
    public  static  final  String Channel_Id="exampleServiceChannel";
    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
      //  startService(new Intent(this,StartSevice.class));
    }
    private void createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=new NotificationChannel(Channel_Id,"Foreground notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager=getSystemService(NotificationManager.class);
            manager.createNotificationChannel(notificationChannel);
        }
    }


}
