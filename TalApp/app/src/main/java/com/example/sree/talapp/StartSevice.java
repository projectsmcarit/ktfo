package com.example.sree.talapp;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

import static com.example.sree.talapp.App.Channel_Id;

public class StartSevice extends Service {
    dataAccess obj;

    @Override
    public void onCreate() {
        super.onCreate();

        //obj.execute();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground();
        Timer timer=new Timer("MyTimer",true);
        timer.scheduleAtFixedRate(new MyRask(), 0,60000);

        return START_STICKY;
    }

    private void startForeground() {

        Intent noti=new Intent(this,MainActivity.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,noti,0);

        startForeground(1,new NotificationCompat.Builder(this,Channel_Id).setOngoing(true)
        .setSmallIcon(R.drawable.ic_launcher_background)
        .setContentTitle("Mobile App")
        .setContentText("App is running in Background")
        .setContentIntent(pendingIntent)
        .build());
    }


    private class MyRask extends TimerTask {
        @Override
        public void run() {
            new dataAccess().execute();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent inte=new Intent(getApplicationContext(),this.getClass());
        inte.setPackage(getPackageName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(inte);
        }
        super.onTaskRemoved(rootIntent);


    }
}
