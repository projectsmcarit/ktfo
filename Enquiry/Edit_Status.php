<?php 
session_start();

$enquiry_no=$_GET['enquiry_no'];
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">location.replace("../index.php");</script>';
}

//Including database connection file
include "../connection.php" ; 

$query = "SELECT * FROM ktfo_enquiry WHERE enquiry_number='$enquiry_no'";
$result = mysqli_query($conn,$query);
while($row=mysqli_fetch_assoc($result))
	{
  		
		$enquiry_no=$row['enquiry_number'];
		$name=$row['name'];				  				    		
        $status=$row['status'];
			
?>
<!DOCTYPE html>
<html>
<head>
    <title>Update Status</title>
    <link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<!--Header-->
    <div class="header" align="center">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
        <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
    </div>
<!--navigation bar-->
    <div class="navbar">
        <a href="../Admin_Home.php">Home</a>
    </div>
    <br><br>
    <div class="form">
        <form action="" method="post">
            <table>
                <tr>
                    <td>Enquiry Number</td>
                    <td><input name="enquiry_no" type="text" disabled="disabled" value="<?php echo $enquiry_no;?>"></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><input name="name" type="text" disabled="disabled" value="<?php echo $name; ?>"></td>
                </tr>
                <tr>
                    <td>Status</td>
	                <td>
		                <input type="text" list="status1" name="status" id="status" required value="<?php echo $status; }?>">
                        <datalist id="status1">
                            <option value="is awaiting Surveyor report">
	                        <option value="is awaiting V.O report">                                                      
                            <option value="is disposed">
                            <option value="is ordered">
                            <option value="is processing">                                                      
	                        <option value="is with Head Surveyor">
	                        <option value="is with JS">
	                        <option value="is with Other Office">
		                    <option value="is with Tahsildar">
		                    <option value="is with Village Office">
		                    <option value="needed permission of DC">
		                    <option value="required additional document">
		                    <option value="required site inspection"> 
                        </datalist>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                    	<button name="update" type="submit" class="btn" onClick="return confirm('Are you sure about the updation?')">Update</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
<?php
	if(isset($_POST['update']))
	{
		$status=$_POST['status'];		
		if($conn->query("UPDATE ktfo_enquiry SET status='$status' WHERE enquiry_number='$enquiry_no'"))
	 	{
	 	    $application_no=0; 
	 		if($result=$conn->query("SELECT name, phone_number, application_number, file_number, date_enquiry FROM ktfo_enquiry WHERE enquiry_number='$enquiry_no';"))
	 		    if($row=$result->fetch_assoc())
	 		    {
	 		    	$name=$row['name'];
	 		    	$phone_number=$row['phone_number'];	 			    
	 		        $application_no=$row['application_number'];
	 		        $file_no=$row['file_number'];
	 		        if (strlen($phone_number)==10) 
	 		        {
	 		        	$message=$phone_number.',Dear '.$name.', your application with file number '.$file_no.' '.$status.'.';
	 		            $array = array($phone_number, $message);
	 		            date_default_timezone_set("Asia/Kolkata");
	 		            $filename=date('Ymdhis',time()).'.csv';
	 		            $file=fopen('sms/'.$filename, 'w') or die("Unable to open file!");
	 		            fwrite($file, $message);
	 		            fclose($file);
	 		        }
	 		        $date=$row['date_enquiry'];
 		            $result1=$conn->query("UPDATE ktfo_application SET status='$status' WHERE application_number='$application_no'");
	 		    }
	        ?>
	        <script>
	        	alert("Updated Successfully");
	        	location.replace("Enquiry_View.php?date=<?php echo $date?>&status=pending&search=search");
	        </script> 
	        <?php	 
		} 
	else
		{
	        ?>
  	        <script> alert("Failed");</script>  
            <?php
		}
	}
?>