<?php
    session_start();
    if(!isset($_SESSION['login_user']))
    {
	    echo "<script>alert('Session Expired');</script>";
	    echo '<script type="text/javascript">location.replace("../index.php");</script>';
    }
   include "../connection.php";
    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Enquiry List</title>
	<link rel="stylesheet" type="text/css" href="../ktfo_css.css">
	<style type="text/css">
		tr td:nth-child(2), tr td:nth-child(4)
		{
            background: #f1f1f1;
        }
        input[type=date]
        {
        	border-radius: 10px;
        }
        input[type=date]
        {
        	font-size: 20px;
        }
        th, td
        {
        	padding: 17px 0px;
        }
        .linkbtn
        {
        	background-color: #f1f1f1;
        	text-decoration: none;
        	color: black;
        	padding: 15px 25px;
        	text-align: center;
        }
        .linkbtn:hover
        {
        	background-color: green;
        	color: white;
        }
        .search
        {
        	padding: 17px 25px;
        	font-size: 15px;
        	display: inline-block;
            border-radius: 10px;
        }
	</style>
</head>
<body>
	<div class="header">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
        <h1>Kottayam Taluk Front Office</h1>
    </div>
	<div class="navbar">
		<a href="../Admin_Home.php">Home</a>
		<a href="../Admin_Application_New/Admin_Application_New_Form.php">New Application</a>
        <a href="../Admin_Pass/Admin_Pass_Generation.php">Issue Pass</a>
		<a href="Enquiry_New.php">New Enquiry</a>
	</div>

    <div id="printdiv" hidden></div>

	<h1 class="header">Enquiry list</h1>
	<div style="width: 100%; text-align: center;">
		<form method="POST">
			<input type="radio" name="status" id="pending" value="pending" checked>
            <label for="pending"><b>Pending</b></label>
            <input type="radio" name="status" id="attended" value="attended">
            <label for="attended"><b>Attended</b></label>
            <br>
			<input type="date" name="date" id="date" style="width: 50%" value="<?php echo date('Y-m-d',time()) ?>">
			<button type="submit" name="search" id="search" class="search">Search</button>
		</form>
	</div>
	<br><br>
	<form method="POST">
		<div id="list" style="width: 100%">
			<table>
				<tr style="text-align: left">
					<th>File Number</th>
					<th>Name</th>
					<th>Phone Number</th>
					<th>Application Subject</th>
					<th>Section</th>
					<th>Status</th>
					<th></th>
				</tr>
				<?php
				    date_default_timezone_set("Asia/Kolkata");
				    $date=date("Y-m-d",time());
				    ?>
				    <script type="text/javascript">
					    var printvalue='<h2 align="center">Pending Enquiry List on <?php echo date("d/m/Y",strtotime($date)); ?><h2><table border="1" width="100%"><tr style="text-align: left"><th>File Number</th><th>Name</th><th>Application Subject</th><th>Section</th><th>Status</th></tr>';
				    </script>
				    <?php	
				    $result=$conn->query("SELECT * FROM ktfo_enquiry WHERE date_enquiry LIKE '$date%' AND status='Pending'");
				    if ($result->num_rows>0)
				    {
				        while($row=$result->fetch_assoc()) 
				    	{
				    		$enquiry_no=$row['enquiry_number'];
							$file_no=$row['file_number'];
				    		$name=$row['name'];
							$phone_no=$row['phone_number'];
				    		$subject=$row['subject'];
				    		$section_id=$row['section_id'];
                            $status=$row['status'];
                            $section="";
                            if($res=$conn->query("SELECT section_name from ktfo_section where section_id='$section_id'"))
                                if($row1=$res->fetch_array())
                                    $section=$row1['section_name'];
				            ?>
				            <tr>
				                <td><?php echo $file_no; ?></td>
				                <td><?php echo $name; ?></td>
								<td><?php echo $phone_no; ?></td>
				                <td><?php echo $subject; ?></td>
				                <td><?php echo $section; ?></td>
				                <td><?php echo $status; ?></td>
				                <td><a href="Edit_Status.php?enquiry_no=<?php echo $enquiry_no; ?>" class="linkbtn">Update Status</a></td>
				            </tr>
				            <script type="text/javascript">
				            	printvalue+='<tr><td><?php echo $file_no; ?></td><td><?php echo $name; ?></td><td><?php echo $subject; ?></td><td><?php echo $section; ?></td><td><input type="text" style="border:none"></td></tr>';
				            </script>
				        <?php
				        }
				        ?>
				        <script type="text/javascript">
				        	printvalue+='</table>';
			    		    document.getElementById('printdiv').innerHTML=printvalue;
				        </script>
				        <tr>
					        <td colspan="7" style="text-align: center;">
						        <button type="button" id="print" style="border-radius: 10px;" onclick="printDiv('printdiv')">PRINT</button>
					        </td>
				        </tr>
				        <?php
				    }
				?>
			</table>
		</div>
	</form>
	<br>
</body>
</html>	

<script type="text/javascript">
	function printDiv(divId) 
	{
        let mywindow = window.open('', 'PRINT', 'height=650,width=900,top=100,left=150');

        mywindow.document.write('<html><head><title>Print</title>');
        mywindow.document.write('</head><body>');
        mywindow.document.write(document.getElementById(divId).innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); 
        mywindow.focus(); 

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>

<?php    
    if (isset($_REQUEST['search'])) 
    {
    	$date=date('Y-m-d',strtotime($_REQUEST['date']));
    	$status1=$_REQUEST['status'];
    	if($status1=='pending')
    	    $result=$conn->query("SELECT * FROM ktfo_enquiry WHERE date_enquiry LIKE '$date%' AND status='Pending';");
    	else
    		$result=$conn->query("SELECT * FROM ktfo_enquiry WHERE date_enquiry LIKE '$date%' AND status<>'Pending';");
    	?>
    	<script type="text/javascript">
            var content='<table><tr style="text-align: left"><th>File Number</th><th>Name</th><th>Phone Number</th<th>Application Subject</th><th>Section</th><th>Status</th><th></th></tr>';
            var printvalue1='<h2 align="center">Pending Enquiry List on <?php echo date("d/m/Y",strtotime($date)); ?><h2><table border="1" width="100%"><tr style="text-align: left"><th>File Number</th><th>Name</th><th>Application Subject</th><th>Section</th><th>Status</th></tr>';
    	</script>
            <?php
    	if ($result->num_rows>0) 
    	{
    		while($row=$result->fetch_assoc()) 
			{
				$enquiry_no=$row['enquiry_number'];
				$file_no=$row['file_number'];
				$name=$row['name'];
				$phone_no=$row['phone_number'];
				$subject=$row['subject'];
				$section_id=$row['section_id'];
                $status=$row['status'];
                $section="";
                if($res=$conn->query("SELECT section_name from ktfo_section where section_id='$section_id'"))
                    if($row1=$res->fetch_array())
                        $section=$row1['section_name'];			
                ?>
			    <script type="text/javascript">
				    content+='<tr><td><?php echo $file_no; ?></td><td><?php echo $name; ?></td><td><?php echo $phone_no; ?></td><td><?php echo $subject; ?></td><td><?php echo $section; ?></td><td><?php echo $status; ?></td><td><a href="Edit_Status.php?enquiry_no=<?php echo $enquiry_no; ?>" class="linkbtn">Update Status</a></td></tr>';
				    printvalue1+='<tr><td><?php echo $file_no; ?></td><td><?php echo $name; ?></td><td><?php echo $subject; ?></td><td><?php echo $section; ?></td><td><input type="text" style="border:none"></td></tr>';

			    </script>
			<?php
			} 
			if($status1=='pending')
			{
			    ?>
			    <script type="text/javascript">
			    	content+='<tr><td colspan="7" style="text-align: center;"><button type="button" id="print" style="border-radius: 10px;" onclick="printDiv(';
			    	content+="'printdiv'";
			    	content+=')">PRINT</button></td></tr>'; 			    		
			    </script>
			<?php
			}              
    	}
    	?>
		<script type="text/javascript">
			content+="</table>";
			printvalue1+='</table>';
			document.getElementById('list').innerHTML=content;
			document.getElementById('printdiv').innerHTML=printvalue1;
			document.getElementById('date').value="<?php echo $date ?>";
			document.getElementById('<?php echo $status1?>').checked=true;
		</script>
		<?php	 
    }

?>