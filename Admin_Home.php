<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("index.php");
			</script>';
}

//Including database connection file
include "connection.php" ; 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Home Page</title>
  <link rel="stylesheet" href="ktfo_home.css">
</head>
<body>
<!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="gvt.jpg" alt="Kerala logo" >
  <h1><b>KOTTAYAM TALUK FRONT OFFICE</b></h1>
  <div class="navbar">
  <a href="#home">Home</a>
  <a href="Admin_Application_New/Admin_Application_New_Form.php">New Application</a>
  <div class="dropdown">
    <button class="dropbtn">Update Application
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="Application/Application_Update_FileNumber.php">Update File Number</a>
      <a href="Application/Application_Update_Status.php">Update Status</a>
    </div>
  </div>
  <a href="Admin_Pass/Admin_Pass_Generation.php">Issue Pass</a>
   <div class="dropdown">
    <button class="dropbtn">Enquiry
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="Enquiry/Enquiry_New.php">New Enquiry</a>
      <a href="Enquiry/Enquiry_View.php">Enquiry List</a>
    </div>
  </div>
  <a href="Application/Admin_Application_Search.php">Edit Application</a>
  <div class="dropdown">
    <button class="dropbtn">Section
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="Section/Admin_Section_View.php">View Section</a>
      <a href="Section/Admin_Section_Add.php">New Section</a>
    </div>
  </div>
  <a href="Logout.php" style="float:right">Logout</a>
  <a href="change_pswrd.php" style="float:right">Change Password</a>
</div>
<br><br>
<?php
$user=$_SESSION['login_user'];
echo "<h2>WELCOME,  ".ucwords($user)." </h2>";
?>

<div style="position:fixed; bottom:0; width:100%;">
<?php
//including footer file
include "Footer.php";
?> 
</div>
</body>
</html>