<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.footer {
   left: 0;
   bottom: 0;
   width: 100%;
   height:40px;
   background-color: #333;
   color: white;
   text-align: center;
}
</style>
</head>
<body>

<div class="footer">
  <p>Developed, hosted and maintained by Dept. of Computer Applications, RIT Kottayam.</p>
</div>

</body>
</html> 
