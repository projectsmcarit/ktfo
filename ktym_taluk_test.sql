-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 17, 2021 at 08:40 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE USER IF NOT EXISTS 'developer'@'localhost' IDENTIFIED BY 'passwd#1234';

CREATE DATABASE IF NOT EXISTS ktym_taluk_test;

GRANT ALL PRIVILEGES ON ktym_taluk_test . * TO 'developer'@'localhost';

USE ktym_taluk_test;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ktym_taluk_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `ktfo_application`
--

DROP TABLE IF EXISTS `ktfo_application`;
CREATE TABLE IF NOT EXISTS `ktfo_application` (
  `application_number` varchar(25) NOT NULL,
  `file_number` varchar(20) DEFAULT NULL,
  `person_id` varchar(15) NOT NULL,
  `section_submitted` int(11) NOT NULL,
  `application_subject` varchar(50) NOT NULL,
  `date_applied` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `section_current` int(11) NOT NULL,
  `list_of_sections` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`application_number`),
  KEY `person_id` (`person_id`),
  KEY `section_current` (`section_current`),
  KEY `section_submitted` (`section_submitted`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ktfo_application`
--

INSERT INTO `ktfo_application` (`application_number`, `file_number`, `person_id`, `section_submitted`, `application_subject`, `date_applied`, `status`, `section_current`, `list_of_sections`) VALUES
('20001', NULL, '101', 45, 'Dummy Value', '2021-01-01 00:00:00', 'fresh', 45, 'NULL'),
('20002', NULL, '106', 24, 'na', '2021-01-01 05:07:29', 'Fresh', 24, ''),
('20003', NULL, '107', 16, 'certificate', '2021-01-01 05:14:52', 'Fresh', 16, ''),
('20004', NULL, '108', 21, 'CERT FOR UNMARRIED NATIVITY CAST', '2021-01-01 05:27:55', 'Fresh', 21, ''),
('20005', NULL, '109', 21, 'certificate FOR ews', '2021-01-01 05:38:47', 'Fresh', 21, '');

-- --------------------------------------------------------

--
-- Table structure for table `ktfo_enquiry`
--

DROP TABLE IF EXISTS `ktfo_enquiry`;
CREATE TABLE IF NOT EXISTS `ktfo_enquiry` (
  `enquiry_number` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `village` varchar(25) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `application_number` varchar(25) DEFAULT NULL,
  `file_number` varchar(25) DEFAULT NULL,
  `date_application` datetime DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `date_enquiry` datetime NOT NULL,
  PRIMARY KEY (`enquiry_number`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ktfo_login`
--

DROP TABLE IF EXISTS `ktfo_login`;
CREATE TABLE IF NOT EXISTS `ktfo_login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ktfo_login`
--

INSERT INTO `ktfo_login` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `ktfo_pass`
--

DROP TABLE IF EXISTS `ktfo_pass`;
CREATE TABLE IF NOT EXISTS `ktfo_pass` (
  `pass_id` int(11) NOT NULL AUTO_INCREMENT,
  `token_number` int(25) NOT NULL,
  `name` varchar(30) NOT NULL,
  `locality` varchar(30) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `visit_purpose` varchar(50) DEFAULT NULL,
  `date_of_pass` datetime NOT NULL,
  PRIMARY KEY (`pass_id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ktfo_pass`
--

INSERT INTO `ktfo_pass` (`pass_id`, `token_number`, `name`, `locality`, `phone_number`, `section_id`, `visit_purpose`, `date_of_pass`) VALUES
(1, 0, '', '', '', 1, 'na', '2021-01-01 05:06:59'),
(2, 0, '', '', '', 25, 'flood', '2021-01-01 06:13:15'),
(3, 0, '', '', '', 19, 'land revenue', '2021-01-02 10:21:02');

-- --------------------------------------------------------

--
-- Table structure for table `ktfo_person`
--

DROP TABLE IF EXISTS `ktfo_person`;
CREATE TABLE IF NOT EXISTS `ktfo_person` (
  `person_id` varchar(15) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `address3` varchar(30) DEFAULT NULL,
  `post_office` varchar(20) DEFAULT NULL,
  `pin_code` int(6) DEFAULT NULL,
  `village` varchar(20) DEFAULT NULL,
  `mobile_number` varchar(10) DEFAULT NULL,
  `email_id` varchar(30) DEFAULT NULL,
  `land_number` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ktfo_person`
--

INSERT INTO `ktfo_person` (`person_id`, `name`, `address1`, `address2`, `address3`, `post_office`, `pin_code`, `village`, `mobile_number`, `email_id`, `land_number`) VALUES
('101', 'Dummy Value', 'Dummy Value', 'Dummy Value', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('102', 'sreekumar', 'dd', 'dd', '', '', 0, '', '', '', ''),
('103', 'jijth', 'jiyth', 'kottayam', '', '', 0, '', '', '', ''),
('104', 'sheril', 'thottakuthu', 'kottayam', '', '', 0, '', '', '', ''),
('105', 'sreekumar', 'kumar', 'kottayam', '', '', 0, '', '', '', ''),
('106', 'sreekumar', 'lalajith', 'kottayam', '', '', 0, '', '', '', ''),
('107', 'muneer', 'thaikudam', 'kottayam', '', '', 0, '', '', '', ''),
('108', 'ATHUL ROBY', 'VALIYAMADA', 'AYMANAM', '', '', 0, '', '', '', ''),
('109', 'ROHITH PRASAD', 'MATTATHIL', 'KOTTAYAM', '', '', 0, '', '', '', ''),
('110', 'nimesh', 'karthika', 'athirampuzjha', '', '', 0, 'ATHIRAMPUZHA', '', '', ''),
('111', 'Tomsy Paul', 'gjhjhgj', 'hgjhgjg', 'kjhkhk', 'mbmnb', 0, '', '', '', ''),
('112', 'nixon', '23', 'ktm', 'pampady', '682597', 0, 'PAMPADY', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ktfo_section`
--

DROP TABLE IF EXISTS `ktfo_section`;
CREATE TABLE IF NOT EXISTS `ktfo_section` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(50) NOT NULL,
  `for_application` char(1) NOT NULL,
  `for_pass` char(1) NOT NULL,
  `section_purpose` varchar(100) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ktfo_section`
--

INSERT INTO `ktfo_section` (`section_id`, `section_name`, `for_application`, `for_pass`, `section_purpose`) VALUES
(1, 'Tahsildar', 'N', 'Y', ''),
(2, 'Tahsildar (LR)', 'N', 'Y', ''),
(3, 'Deputy Tahsildar (HQ)', 'N', 'N', ''),
(4, 'Deputy Tahsildar Election', 'N', 'N', ''),
(5, 'Head Surveyor', 'N', 'N', ''),
(6, 'Surveyor_________________', 'N', 'N', ''),
(7, 'Deputy Tahsildar - A', 'N', 'N', ''),
(8, 'Deputy Tahsildar - B', 'N', 'N', ''),
(9, 'Deputy Tahsildar - C', 'N', 'N', ''),
(10, 'Deputy Tahsildar - D', 'N', 'N', ''),
(11, 'Deputy Tahsildar - E', 'N', 'N', ''),
(12, 'Deputy Tahsildar - F', 'N', 'N', ''),
(13, 'Deputy Tahsildar - G', 'N', 'N', ''),
(14, 'Deputy Tahsildar - H', 'N', 'N', ''),
(15, 'A1', 'Y', 'N', 'Establishment'),
(16, 'A2', 'Y', 'N', 'Compassionate employment'),
(17, 'A3', 'Y', 'N', ''),
(18, 'A4', 'Y', '', ''),
(19, 'A5', 'Y', '', ''),
(20, 'A6', 'Y', '', ''),
(21, 'B1', 'Y', 'N', 'Certificate'),
(22, 'B2', 'Y', 'N', 'Arms and Explosive/mining, Electricity, Covid-19, Law and order'),
(23, 'B3', 'Y', 'N', 'Tree cutting, Birth/death registration, Miscellaneous'),
(24, 'C1', 'Y', 'N', 'Land transfer'),
(25, 'C2', 'Y', 'N', 'Disaster management'),
(26, 'C3', 'Y', 'N', 'CMDRF'),
(27, 'C4', 'Y', 'N', 'NFBS'),
(28, 'D1', 'Y', 'N', 'Luxury tax'),
(29, 'D2', 'Y', '', ''),
(30, 'D3', 'Y', '', ''),
(31, 'D4', 'Y', '', ''),
(32, 'D5', 'Y', '', ''),
(33, 'F1', 'Y', 'N', 'Encroachment of land,Etiction'),
(34, 'F2', 'Y', 'N', 'Land assignment, Pattayam'),
(35, 'F3', 'Y', 'N', 'Surplus land'),
(36, 'F4', 'Y', '', ''),
(37, 'G1', 'Y', 'N', 'Resurvey correction, PV, Point out of boundary, Area alteration'),
(38, 'G2', 'Y', '', ''),
(39, 'G3', 'Y', '', ''),
(40, 'G4', 'Y', '', ''),
(41, 'H1', 'Y', '', ''),
(42, 'J1', 'Y', 'N', 'Election'),
(43, 'K1', 'Y', 'N', 'CMO files'),
(44, 'K2', 'Y', 'N', 'Revenue recovery, Adalath'),
(45, 'K3', 'Y', 'N', 'KLU(Land utilization),Paddy and wetland act, Land conversion'),
(50, 'E2', 'Y', 'N', 'Pensions'),
(51, 'E3', 'Y', 'N', 'P V cancellation'),
(52, 'E4', 'Y', 'N', 'Legal heirship certificate'),
(53, 'E5', 'Y', 'N', ''),
(54, 'E1', 'Y', 'N', ''),
(55, 'B4', 'Y', 'N', 'Pumping subsidy, RIT(Right to Information Act), Right to service, Auction'),
(56, 'B5', 'Y', 'N', 'Irrigation');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ktfo_application`
--
ALTER TABLE `ktfo_application`
  ADD CONSTRAINT `ktfo_application_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ktfo_person` (`person_id`),
  ADD CONSTRAINT `ktfo_application_ibfk_2` FOREIGN KEY (`section_current`) REFERENCES `ktfo_section` (`section_id`),
  ADD CONSTRAINT `ktfo_application_ibfk_3` FOREIGN KEY (`section_submitted`) REFERENCES `ktfo_section` (`section_id`);

--
-- Constraints for table `ktfo_enquiry`
--
ALTER TABLE `ktfo_enquiry`
  ADD CONSTRAINT `ktfo_enquiry_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `ktfo_section` (`section_id`);

--
-- Constraints for table `ktfo_pass`
--
ALTER TABLE `ktfo_pass`
  ADD CONSTRAINT `ktfo_pass_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `ktfo_section` (`section_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
