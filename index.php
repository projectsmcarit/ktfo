<?php
   session_start();
   global $error;
   // connection file included
   //Replacing with include $conn=mysqli_connect("localhost","root","","ktym_taluk");
   include("connection.php");
   if($_SERVER["REQUEST_METHOD"] == "POST") 
   {
   // username and password sent from form

      $myusername = mysqli_real_escape_string($conn,$_POST['username']);
      $mypassword = mysqli_real_escape_string($conn,$_POST['password']);

      $selectquerry = "SELECT username FROM ktfo_login WHERE username = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($conn,$selectquerry);
      $row = mysqli_fetch_array($result);
      $num_row = mysqli_num_rows($result);

      // If result matched $myusername and $mypassword, table row must be 1 row

      if($num_row == 1) 
	   {
         //session_register("myusername");
         $_SESSION['login_user'] = $myusername;

         header("location: Admin_Home.php");
      }
	   else 
	   {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
<!DOCTYPE html>
<html>
<head>
   <title>Kottayam Taluk Front Office</title>
   <script type="text/javascript">
      function preventForward()
      {
         window.history.forward();
      }
      setTimeout("preventBack()", 5*60*1000);
      window.onunload = function () { null };
   </script>
   <link rel="stylesheet" href="ktfo_css.css">
   <style>
      #login_bt
      {
	      background-color:#999999;color:#FFFFFF;width:300px;
      }
      #login_bt:hover { background-color:#009900;}
      .container
      {
	      border:2px solid black;
	      border-collapse:collapse;
	      margin: auto;
         width: 350px;
	      padding: 10px;
      }
      input[type=text],input[type=password]
      {
	      font-family:Arial, Helvetica, sans-serif;
	      font-size: 14px;
  	      width: 100%;
  	      padding: 12px 20px;
  	      margin: 8px 0;
  	      display: inline-block;
  	      border: 1px solid #ccc;
  	      border-radius: 4px;
  	      box-sizing: border-box;
      }
      .error
      {
	      background-color:#FF99FF;
	      color:#FF0000;
      }
   </style>
</head>

<body>
<!--Header-->
   <div class="header" align="center">
      <img class="site_logo" height="100" id="logo" src="gvt.jpg" alt="Kerala logo" >
      <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
   </div>
   <div align="center">
      <div class="container">
         <form name="login" method="post" action="">
            <table style="width:100%; height:200px;">
               <tr>
                  <td align="center"><input name="username" type="text" placeholder="Enter Username"></td>
               </tr>
               <tr>
                  <td align="center"><input name="password" type="password" placeholder="Enter Password"></td>
               </tr>
               <tr>
                  <td align="center"><button id="login_bt" name="submit" type="submit">LOGIN</button></td>
               </tr>
            <div class="error"><?php echo $error; ?><div>
            </table>
         </form>
      </div>
   </div>
</body>
</html>
