<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php" 
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../ktfo_css.css">
</head>
<body>
	<div class="header">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
        <h1>Kottayam Taluk Front Office</h1>
    </div>
	<div class="navbar">
		<a href="../Admin_Home.php">Home</a>
		<a href="../Admin_Pass/Admin_Pass_Generation.php">Issue Pass</a>
		<a href="../Enquiry/Enquiry_New.php">New Enquiry</a>
	</div>
	<h1 class="header">NEW APPLICATION</h1>
	<div class="form">
	    <form name="form" method="POST">
	        <table>
	        	<tr>
	        		<td>
	        			<label for="file_no">File Number:</label>
	        		</td>
	        		<td>
	        			<input type="text" name="file_no" id="file_no" value="" pattern="[A-Za-z0-9/-]+">
	        		</td>
	        	</tr>
		        <tr>
		        	<td>
		        		<label for="name">Name:<span>*</span></label>
			        </td>
			        <td>
				        <input type="text" id="name" name="name" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="house">House Name/Number:<span>*</span></label>
			        </td>
			        <td>
				        <input type="text" id="house" name="house" required>
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="loc">Locality:<span>*</span></label>
			        </td>
			        <td>
				        <input type="text" id="loc" name="loc" required>
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="city">City:</label>
			        </td>
			        <td>
				        <input type="text" id="city" name="city" value="">
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="vlg">Village:</label>
			        </td>
			        <td>
				        <select name="vlg" id="vlg">
					        <option value="">Select</option>
					        <option value="OMANTHURUTHU">OMANTHURUTHU</option>	
                            <option value="KAIPUZHA">KAIPUZHA</option> 
                            <option value="ATHIRAMPUZHA">ATHIRAMPUZHA</option>
                            <option value="ETTUMANOOR">ETTUMANOOR</option>
                            <option value="PEROOR">PEROOR</option>
                            <option value="ARPOOKARA">ARPOOKARA</option>
                            <option value="AYMANAM">AYMANAM</option>
                            <option value="KUMARAKOM">KUMARAKOM</option>
                            <option value="CHENGHALAM SOUTH">CHENGHALAM SOUTH</option>
                            <option value="THIRUVARPPU">THIRUVARPPU</option>
                            <option value="PERUMBAIKKADU">PERUMBAIKKADU</option>
                            <option value="VIJAYAPURAM">VIJAYAPURAM</option>
                            <option value="MUTTAMBALAM">MUTTAMBALAM</option>
                            <option value="KOTTAYAM">KOTTAYAM</option>
                            <option value="VELOOR">VELOOR</option>
                            <option value="NATTAKOM">NATTAKOM</option>
                            <option value="PANACHICKADU">PANACHICKADU</option>
                            <option value="AYARKUNNAM">AYARKUNNAM</option>
                            <option value="AKALAKUNNAM">AKALAKUNNAM</option> 
                            <option value="CHENGALAM EAST">CHENGALAM EAST</option>
                            <option value="KOOROPPADA">KOOROPPADA</option>
                            <option value="MANARKADU">MANARKADU</option>
                            <option value="PAMPADY">PAMPADY</option>
                            <option value="PUTHUPPALLY">PUTHUPPALLY</option>
                            <option value="MEENADOM">MEENADOM</option>
                            <option value="ANIKKADU">ANIKKADU</option>
				        </select>
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="po">Post Office:</label>
			        </td>
			        <td>
				        <input type="text" id="po" name="po" value="">
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="pc">Pincode:</label>
			        </td>
			        <td>
				        <input type="text" id="pc" name="pc" pattern="[0-9]{6}" title="Only 6 digits are allowed" value="">
			        </td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="phn">Phone Number:</label>
		        	</td>
		        	<td>
		        		<input type="tel" id="phn" name="phn" pattern="[0-9]{10}" title="Only 10 digits are allowed" value="">
		        	</td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="ln">Land Number:</label>
		        	</td>
		        	<td>
		        		<input type="text" id="ln" name="ln" pattern="[0-9]+" title="Only digits are allowed" value="">
		        	</td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="email">Email-id:</label>
		        	</td>
		        	<td>
		        		<input type="email" id="email" name="email" placeholder="Eg: abc@gmail.com" value="">
		        	</td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="sec">Initial Section<span>*</span></label>
		        	</td>
		        	<td>
                       <?php
                            $sql="SELECT section_id, section_name from ktfo_section where for_application='Y' order by section_name;";
                            $result=$conn->query($sql);
                        ?>    
                        <select id="section" name="section" required title="Select an initial section">
                            <option value="">Select</option>
                        <?php                            
                            if($result->num_rows>0)
                                while($row=$result->fetch_assoc())
                                    echo '<option value="'.$row['section_id'].'">'.$row['section_name'].'</option>';                    
                        ?> 
                        </select>   
                    </td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="sub">Application Subject:<span>*</span></label>
		        	</td>
		        	<td>
		        		<input type="text" id="sub" name="sub" required value="">
		        	</td>
		        </tr>
		        <tr>
			        <td colspan="2" class="buttontd">
				        <button type="reset">Reset</button>
				        <button type="submit" name="submit" class="submit">Submit</button>
			        </td>
		        </tr>
	        </table>
        </form><br>
		
    </div>
	<?php
			//including footer file
			include "../Footer.php";
		?>
	
    <?php
        if (isset($_POST['submit'])) 
        {   
            $file_no=$_POST['file_no'];     	
        	$name=$_POST['name'];
        	$house=$_POST['house'];
        	$loc=$_POST['loc'];
        	$city=$_POST['city'];
        	$vlg=$_POST['vlg'];
        	$po=$_POST['po'];
        	$pc=intval($_POST['pc']);
        	$phn=$_POST['phn'];
        	$ln=$_POST['ln'];
        	$email=$_POST['email'];
        	$section=$_POST['section'];
        	$sub=$_POST['sub'];
        	date_default_timezone_set("Asia/Kolkata");
            $date=date("Y-m-d h:i:s",time());
        	

        	$sql5="SELECT person_id FROM ktfo_person where name='$name' and address1='$house' and address2='$loc';";
            $result3=$conn->query($sql5);
            if($result3->num_rows>0)       		
        	{	
        		if($row3=$result3->fetch_assoc())
        		{
        			$person_id=intval($row3['person_id']);	
        		}
        		$sql2='SELECT application_number FROM ktfo_application ORDER BY CAST(application_number as int) DESC LIMIT 1';
        		if($result2=$conn->query($sql2))
        	    {
        		    if($row2=$result2->fetch_assoc())
        		    {  
        			    $application_number=intval($row2['application_number'])+1;
        		        $sql4="INSERT INTO ktfo_application (application_number, file_number, person_id, section_submitted, application_subject, date_applied, status, section_current) VALUES ('$application_number','$file_no','$person_id',$section,'$sub','$date','Fresh',$section);";
        		    }
        	    }
        	    if($conn->query($sql4))
	            { 
	?>
		            <script type="text/javascript"> 
					    alert("Successful");
                        window.onload=function() 
                        {
                        	window.open('Admin_Application_New_Receipt.php');
                        }
					</script>
    <?php	  
                } 
                else
                {
	?>
                    <script type="text/javascript"> 
					    alert("Failed");
                        location.replace("Admin_Application_New_Form.php");
                    </script>
    <?php
                }
        	}
        	else
        	{
        		$sql1='SELECT person_id FROM ktfo_person ORDER BY CAST(person_id as int) DESC LIMIT 1';
        	    $sql2='SELECT application_number FROM ktfo_application ORDER BY CAST(application_number as int) DESC LIMIT 1';
     
        	    if($result1=$conn->query($sql1))        		
        	    {	
        		    if($row1=$result1->fetch_assoc())
        		    {
        			    $person_id=intval($row1['person_id'])+1;
        		        $sql3="INSERT INTO ktfo_person (person_id, name, address1, address2, address3, post_office, pin_code, village, mobile_number, email_id, land_number) VALUES ('$person_id','$name','$house','$loc','$city','$po',$pc,'$vlg','$phn','$email','$ln');";	
        		    }
        	    }	
        	
        	    if($result2=$conn->query($sql2))
        	    {
        		    if($row2=$result2->fetch_assoc())
        		    {
        			    $application_number=intval($row2['application_number'])+1;
        		        $sql4="INSERT INTO ktfo_application (application_number, file_number, person_id, section_submitted, application_subject, date_applied, status, section_current) VALUES ('$application_number','$file_no','$person_id',$section,'$sub','$date','Fresh',$section);";
        		    }
        	    }
        	    if($conn->query($sql3)&&$conn->query($sql4))
	            { 	            	
	?>
		            <script type="text/javascript"> 
					    alert("Successful");
                        window.onload=function() 
                        {
                        	window.open('Admin_Application_New_Receipt.php');
                        }
                    </script>   
    <?php	  
                } 
                else
                {
	?>
                    <script type="text/javascript"> 
					    alert("Failed");
                        location.replace("Admin_Application_New_Form.php");
                    </script>
    <?php
                }
        	}
        }
    ?> 	
</body>
</html>