<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//including database connection file
include "../connection.php" ; 

$section_id=$_GET['section_id'];
//fetching data from table ktfo_section
$qry="select * from ktfo_section where section_id='$section_id'";
$result=$conn->query($qry);
if($result->num_rows>0)
{
	while($data=$result->fetch_assoc())
	{

?>
<!DOCTYPE html>
<html>
<head>
<title>Admin_Section_Edit</title>
<link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<script>
	<!--confirmation message before delete section-->
	function update()
	{
		return confirm("Do you really want to update");
	}
	<!--validation to enter Y / N for_application field-->
	function YN_validation()
	{
		var value= document.getElementById("for_application").value;
		if(value=="Y" || value=="N" )
		{ return;}
		else
		{
			document.getElementById("for_application").value="";
			window.alert("You should enter Y or N ");
		}
	}
	function YN_validation()
	{
		var value= document.getElementById("for_pass").value;
		if(value=="Y" || value=="N" )
		{ return;}
		else
		{
			document.getElementById("for_pass").value="";
			window.alert("You should enter Y or N ");
		}
	}
</script>
<!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
  <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar -->
<div class="navbar">
<a href="../Admin_Home.php">Home</a>
</div>
<br><br>
<div class="form">
<form id="section_edit" name="section_edit" method="post" action="" onSubmit="return update()">
  <table>
  	<tr>
      <td>Section ID</td>
      <td><input type="text" name="section_id" disabled="disabled" value="<?php echo $data['section_id'];?>"/></td>
    </tr>
	<tr>
      <td>Section Name<span> * </span></td>
      <td><input type="text" name="section_name" value="<?php echo $data['section_name'];?>" required /></td>
    </tr>
	<tr>
      <td>For Application<span> * </span>(Y/N)</td>
      <td><input type="text" id="for_application" name="for_application" onBlur="YN_validation()" value="<?php echo $data['for_application'];?>" required /></td>
    </tr>
	<tr>
	  <td>For Pass<span> * </span>(Y/N)</td>
      <td><input type="text" id="for_pass" name="for_pass" onBlur="YN_validation()"value="<?php echo $data['for_pass'];?>"/></td>
    </tr>
	<tr>
      <td>Section Purpose</td>
      <td><textarea name="purpose" cols="5" rows="5"><?php echo $data['section_purpose']; }}?></textarea></td>
    </tr>
    <tr>
	<td colspan="2" align="center"><button type="reset" name="cancel" onClick="window.location='Admin_Section_View.php';return false;">CANCEL</button>
	  	 <button type="submit" name="edit">UPDATE</button></td>
    </tr>
  </table>
</form>
</div>

<div style=" bottom:0; width:100%;">
<?php
//including footer file
include "../Footer.php";
?> 
</div>
</body>
</html>
<?php
 if(isset($_POST['edit']))
 {
 	$section=$_POST['section_name'];
	$for_application=$_POST['for_application'];
	$for_pass=$_POST['for_pass'];
	$purpose=$_POST['purpose'];
	//updating section_name 
	$sql="update ktfo_section set section_name='$section' , for_application='$for_application', for_pass='$for_pass', section_purpose='$purpose' where section_id='$section_id'";
	
	if($conn->query($sql)== TRUE)
	 	{ 
	?>
	<script>confirm(" Updated Successfully");</script> 
	<?php	
		header('location:Admin_Section_View.php');  
		} 
	else
		{
	?>
  	<script> alert("failed");</script>  
<?php
		}
}
?>