<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}
//Including database connection file
include "connection.php" ; 
?>
<!DOCTYPE html>
<html>
<head>
<title>Change Password</title>

<!--styles file including-->
<link rel="stylesheet" href="ktfo_css.css">
<style>
input[type=password]
{
	font-family:Arial, Helvetica, sans-serif;
	font-size: 14px;
  	width: 100%;
  	padding: 12px 20px;
  	margin: 8px 0;
  	display: inline-block;
  	border: 1px solid #ccc;
  	border-radius: 4px;
  	box-sizing: border-box;
}
</style>
</head>
<body>
<!--Header-->
<div class="header" align="center">
  <img class="site_logo" height="100" id="logo" src="gvt.jpg" alt="Kerala logo" >
  <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
</div>
<!--navigation bar -->
<div class="navbar">
<a href="Admin_Home.php">Home</a>
</div>
<br><br>
<!--form to add new section-->
<h3 align="center">Change Your Password</h3>
<div class="form" >
<form id="change_pswd" name="change_pswd" method="post" action="" onSubmit="return submit_form()">
  <table>
    <tr>
      <td>User Name<span> * </span></td>
      <td><input type="text" name="user_name" required="required"/></td>
    </tr>
	<tr>
      <td>Current Password<span> * </span></td>
      <td><input type="password" name="crnt_pswd" required="required"/></td>
    </tr>
	<tr>
      <td>New Password<span> * </span></td>
      <td><input type="password" id="new_pswd" name="new_pswd" required="required"/></td>
    </tr>
	  <td>Confirm Password<span> * </span></td>
      <td><input type="password" id="cnfm_pswd" name="cnfm_pswd" required="required" /></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><button type="cancel" onClick="window.location='Admin_Home.php';return false;">CANCEL</button>
	  	 <button type="submit" name="change">Change</button></td>
    </tr>
  </table>
</form>
<br><br><br><br>
</div>
<div style="position:fixed; bottom:0; width:100%;">
<?php
//including footer file
include "Footer.php";
?> 
</body>
</html>
<?php
 if(isset($_POST['change']))
 {
 	$user_name=$_POST['user_name'];
	$crnt_pswd=$_POST['crnt_pswd'];
	$new_pswd=$_POST['new_pswd'];
	$cnfm_pswd=$_POST['cnfm_pswd'];
	//insert new section to table ktfo_section
	$sql="select * from ktfo_login where username='$user_name' and password='$crnt_pswd'";
	$res=mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($res)>0)
	{
		if($new_pswd==$cnfm_pswd)
		{
			$sql2="UPDATE ktfo_login SET password='$cnfm_pswd' WHERE username='$user_name'";
			if(mysqli_query($conn,$sql2))
			{
				echo "<script>alert('Password Changed!');</script>";
				echo '<script type="text/javascript">
				location.replace("index.php");
				</script>';
			}
			else
			{
				echo "<script>alert('Something Wrong!!!! Password dosen't Changed.Try Again...');</script>";
				echo '<script type="text/javascript">
				location.replace("Admin_Home.php");
				</script>';
			}
		}
		else
		{
			echo "<script>
			alert('Password doesn't match...!');
			window.location = 'Admin_Home.php';
		   </script>";
		}

	}
	else
	{
		echo "<script>alert('Something Wrong!!!! Username or Password incorrect');</script>";
		echo '<script type="text/javascript">
		location.replace("Admin_Home.php");
		</script>';
	}
	
 }
?>