<?php
   include "../connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width,initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="stylesheet" href="bootstrap.css">
   <link rel="stylesheet" href="main.css">
   <title>Public search portal</title>
   <style type="text/css">
      input[type=submit]:hover
      {
         background-color: limegreen;
         color: white;
      }
   </style>
</head>
<body>
   <div class="container">
      <div class="card">
         <img class="img_container" height="100" id="logo" src="../gvt.jpg" alt="Kerala Logo">
         <p><b>Serach your application status..!</b></p>
         <form method="GET">
            <input type="text" name="application_number" placeholder="Enter Your Application Number"><br/>
            <input type="submit" value="submit" name="submit">
         </form>
      </div>
   </div>
   <div style="position:fixed; bottom:0; width:100%;">
         <?php
         //including footer file
         include "../Footer.php";
         ?> 
   </div>
</body>
</html>
<?php
   if (isset($_REQUEST['submit'])) 
   {
      $application_no=$_REQUEST['application_number'];
      $result=$conn->query("SELECT application_number FROM ktfo_application WHERE application_number='$application_no';");
      if($result->num_rows>0) 
      {
         header("location:public_search_2.php?application_number=$application_no&submit=submit");
      }
      else
      {
         ?>
         <script type="text/javascript">
            location.replace("public_search_1.php");
            alert("Invalid Application Number!!!");
         </script>
         <?php
      }
   } 
?>
