<?php 
session_start();
if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//including connection file
    include "../connection.php" 
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!--including css file-->
	<link rel="stylesheet" type="text/css" href="../ktfo_css.css">
</head>
<body>
	<div class="header">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo">
        <h1>Kottayam Taluk Front Office</h1>
    </div>
	<div class="navbar">
		<a href="../Admin_Home.php">Home</a>
		<a href="../Admin_Application_New/Admin_Application_New_Form.php">New Application</a>
		<a href="../Enquiry/Enquiry_New.php">New Enquiry</a>
	</div>
	<h1 class="header">GENERATE PASS</h1>
	<div class="form">
	    <form name="form" method="POST">
	        <table>
		        <tr>
		        	<td>
		        		<label for="name">Name:<span>*</span></label>
			        </td>
			        <td>
				        <input type="text" id="name" name="name" required pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable">
			        </td>
		        </tr>
		        <tr>
			        <td>
				        <label for="loc">Locality:</label>
			        </td>
			        <td>
				        <input type="text" id="locality" name="locality" value="">
			        </td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="phn">Phone Number:</label>
		        	</td>
		        	<td>
		        		<input type="tel" id="phone_number" name="phone_number" pattern="[0-9]{10}" title="Only 10 digits are allowed" value="">
		        	</td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="section">Section:</label>
		        	</td>
		        	<td>
		        		<?php
                            $sql="select section_id, section_name from ktfo_section where for_pass='Y';";
                            $result=$conn->query($sql);
                        ?>    
                            <select id="section" name="section">
                            	<option value="" selected>Select</option>
                        <?php    	
                            if($result->num_rows>0)
	                            while($row=$result->fetch_assoc())
		                            echo '<option value="'.$row['section_id'].'">'.$row['section_name'].'</option>';
                            echo '</select>';
                        ?>    
		        	</td>
		        </tr>
		        <tr>
		        	<td>
		        		<label for="purpose">Visit Purpose:</label>
		        	</td>
		        	<td>
		        		<input type="text" id="purpose" name="purpose" value="">
		        	</td>
		        </tr>
		        <tr>
			        <td colspan="2" class="buttontd">
				        <button type="reset">Reset</button>
				        <button type="submit" name="submit" class="submit">Submit</button>
			        </td>
		        </tr>
	        </table>
        </form>
    </div>
    
    <?php
    $sql1='SELECT token_number, date_of_pass FROM ktfo_pass ORDER BY pass_id DESC LIMIT 1';     
        if (isset($_POST['submit'])) 
        {        	
        	$name=$_POST['name'];
        	$locality=$_POST['locality'];
        	$phone_number=$_POST['phone_number'];
        	$section=$_POST['section'];
        	$purpose=$_POST['purpose'];
        	date_default_timezone_set("Asia/Kolkata");
        	$date=date("Y-m-d h:i:s",time());
        	$sql1='SELECT token_number, date_of_pass FROM ktfo_pass ORDER BY pass_id DESC LIMIT 1';     
        	$result1=$conn->query($sql1);       		
        	if($row1=$result1->fetch_assoc())
        	{
        		if(date("Y-m-d",strtotime($row1['date_of_pass']))==date("Y-m-d",strtotime($date)))
        		    $token_number=$row1['token_number']+1;
        		else
        			$token_number=1;
        	}
        	if ($section=="") 
        	    $sql2="INSERT INTO ktfo_pass (token_number, name, locality, phone_number, visit_purpose, date_of_pass) VALUES ($token_number,'$name','$locality','$phone_number','$purpose','$date');";   	    	
        	else
        	    $sql2="INSERT INTO ktfo_pass (token_number, name, locality, phone_number, section_id, visit_purpose, date_of_pass) VALUES ($token_number,'$name','$locality','$phone_number',$section,'$purpose','$date')";      	           	
        	if($conn->query($sql2))
	        { 	            	
	?>
		        <script type="text/javascript"> 
					alert("Successful");
                    window.onload=function() 
                    {
                        window.open('Pass_Receipt.php');
                    }
                </script>   
    <?php	  
            } 
            else
            {
	?>
                <script type="text/javascript"> 
					alert("Failed");
                    location.replace("Admin_Pass_Generation.php");
                </script>
    <?php
            }
        }
		//including footer file
		include "../Footer.php";
	?>
</body>
</html>