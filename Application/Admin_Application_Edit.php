<?php 
   session_start();
   if(!isset($_SESSION['login_user']))
   {
      echo "<script>alert('Session Expired');</script>";
      echo '<script type="text/javascript">
            location.replace("../index.php");
            </script>';
   }
   //Including database connection file
   include "../connection.php" ; 
?>

<!DOCTYPE html>
<html>
<head>
<!--styles file including-->
   <link rel="stylesheet" href="../ktfo_css.css">
   <style type="text/css">
      form 
      {
         width: 80%;
         margin: 20px auto;
         text-align: left;
         padding: 10px;
         border: 3px solid #f1f1f1;
         border-radius: 3px;
      }
      .input-group 
      {
         margin: 10px 0px 10px 0px;
         font-size: 20px;
      }
      .input-group label 
      {
         display: block;
         text-align: left;
         margin: 3px;
      }
      .btn
      {
         padding: 5px;
         font-size: 25px;
         color: white;
         background:skyblue ;
         border:none;
         border-radius: 3px;
      }
   </style>
</head>
<body>
   <!--Header-->
   <div class="header" align="center">
      <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
      <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
   </div>
   <!--navigation bar -->
   <div class="navbar">
      <a href="../Admin_Home.php">Home</a>
   </div>
   <br><br>
   <center><h2><b>Edit Application</b></h2></center>
<?php
   if(isset($_POST['edit']))
   { 
      $application_number=$_POST['application_number'];
      $query1="SELECT application_subject, date_applied FROM ktfo_application WHERE application_number='$application_number'";
      $result1=$conn->query($query1);
      $query2="SELECT * FROM ktfo_person WHERE person_id=(SELECT person_id FROM ktfo_application WHERE application_number='$application_number')";
      $result2=$conn->query($query2);
      if ($row1=$result1->fetch_assoc()) 
      {
         $application_subject=$row1['application_subject'];
         $date_applied=$row1['date_applied'];
      }
      if($row2=$result2->fetch_assoc())
      {    
         $person_id=$row2['person_id'];    
         $name=$row2['name']; 
         $mobile_number=$row2['mobile_number'];
         $address1=$row2['address1'];
         $address2=$row2['address2'];
         $address3=$row2['address3'];
         $post_office=$row2['post_office'];
         $pin_code=$row2['pin_code'];
         $village=$row2['village'];
         $email_id=$row2['email_id'];
         $land_number=$row2['land_number'];
      }
   }   
?>
   <form name="form2" method="POST">
      <table  width=90%>
         <div class="input-group">
            <tr>
               <td><label>Application Number:</label></td>
               <td><input type="text" name="application_number" required  value="<?php echo (isset($application_number)&&!empty($application_number)) ? $application_number : '';?>"readonly></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Application Subject:</label></td>
               <td><input type="text" name="application_subject" required value="<?php echo (isset($application_subject)&&!empty($application_subject)) ? $application_subject : '';?>"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Date Applied:</label></td>
               <td><input type="date" name="date_applied" disabled="disabled" value="<?php echo (isset($date_applied)&&!empty($date_applied)) ? date('Y-m-d', strtotime($date_applied)) : ''; ?>"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Name:</label></td>
               <td><input type="text" name="name" required value="<?php echo (isset($name)&&!empty($name)) ? $name : '';?>" pattern="[A-Za-z ]+" title="Your name is not valid. Only characters A-Z, a-z, space are acceptable"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>House Name/Number:</label></td>
               <td><input type="text" name="address1" required value="<?php echo (isset($address1)&&!empty($address1)) ? $address1 : '';?>"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Locality</label></td>
               <td><input type="text" name="address2" required value="<?php echo (isset($address2)&&!empty($address2)) ? $address2 : '';?>"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>City:</label></td>
               <td><input type="text" name="address3" value="<?php echo (isset($address3)&&!empty($address3)) ? $address3 : ''; ?>"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Village:</label></td>
               <td>
                  <select name="vlg" id="vlg">
                     <option value="">Select</option>
                     <option value="OMANTHURUTHU">OMANTHURUTHU</option>  
                     <option value="KAIPUZHA">KAIPUZHA</option> 
                     <option value="ATHIRAMPUZHA">ATHIRAMPUZHA</option>
                     <option value="ETTUMANOOR">ETTUMANOOR</option>
                     <option value="PEROOR">PEROOR</option>
                     <option value="ARPOOKARA">ARPOOKARA</option>
                     <option value="AYMANAM">AYMANAM</option>
                     <option value="KUMARAKOM">KUMARAKOM</option>
                     <option value="CHENGHALAM SOUTH">CHENGHALAM SOUTH</option>
                     <option value="THIRUVARPPU">THIRUVARPPU</option>
                     <option value="PERUMBAIKKADU">PERUMBAIKKADU</option>
                     <option value="VIJAYAPURAM">VIJAYAPURAM</option>
                     <option value="MUTTAMBALAM">MUTTAMBALAM</option>
                     <option value="KOTTAYAM">KOTTAYAM</option>
                     <option value="VELOOR">VELOOR</option>
                     <option value="NATTAKOM">NATTAKOM</option>
                     <option value="PANACHICKADU">PANACHICKADU</option>
                     <option value="AYARKUNNAM">AYARKUNNAM</option>
                     <option value="AKALAKUNNAM">AKALAKUNNAM</option> 
                     <option value="CHENGALAM EAST">CHENGALAM EAST</option>
                     <option value="KOOROPPADA">KOOROPPADA</option>
                     <option value="MANARKADU">MANARKADU</option>
                     <option value="PAMPADY">PAMPADY</option>
                     <option value="PUTHUPPALLY">PUTHUPPALLY</option>
                     <option value="MEENADOM">MEENADOM</option>
                     <option value="ANIKKADU">ANIKKADU</option>
                  </select>
               </td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Post Office:</label></td>
               <td><input type="text" name="post_office" value="<?php echo (isset($post_office)&&!empty($post_office)) ? $post_office : ''; ?>"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Pincode:</label></td>
               <td><input type="text" name="pin_code" value="<?php echo (isset($pin_code)&&!empty($pin_code)) ? $pin_code : ''; ?>" pattern="[0-9]{6}" title="Only 6 digits are allowed"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Mobile Number:</label></td>
               <td><input type="text" name="mobile_number" value="<?php  echo (isset($mobile_number)&&!empty($mobile_number)) ? $mobile_number : ''; ?>" pattern="[0-9]{10}" title="Only 10 digits are allowed"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Land Number:</label></td>
               <td><input type="text" name="land_number" value="<?php echo (isset($land_number)&&!empty($land_number)) ? $land_number : ''; ?>" pattern="[0-9]+" title="Only digits are allowed"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td><label>Email:</label></td>
               <td><input type="email" name="email_id" value="<?php echo (isset($emai_id)&&!empty($email_id)) ? $email_id : '';?>" placeholder="Eg: abc@gmail.com"></td>
            </tr>
         </div>
         <div class="input-group">
            <tr>
               <td colspan="2">
                  <br><br>
                  <center>
                     <button type="submit" name="update" class="btn">Update</button>
                  </center>
               </td>
            </tr>   
         </div>
      </table>   
   </form>
</body>
</html>
<?php
   if(isset($_POST['update']))
   {
      $application_number=$_POST['application_number']; 
      $application_subject=$_POST['application_subject']; 
      $name=$_POST['name']; 
      $mobile_number=$_POST['mobile_number'];
      $address1=$_POST['address1'];
      $address2=$_POST['address2'];
      $address3=$_POST['address3'];
      $post_office=$_POST['post_office'];
      $pin_code=intval($_POST['pin_code']);
      $village=$_POST['village'];
      $email_id=$_POST['email_id'];
      $land_number=$_POST['land_number'];
      $query1="UPDATE ktfo_application SET application_subject='$application_subject' WHERE application_number='$application_number';";    
      $result1=mysqli_query($conn,$query1);
      $query2="UPDATE ktfo_person SET name='$name',mobile_number='$mobile_number',address1='$address1',address2='$address2',address3='$address3',post_office='$post_office',pin_code=$pin_code,village='$village',email_id='$email_id',land_number='$land_number' WHERE person_id=(SELECT person_id FROM ktfo_application WHERE application_number='$application_number');";
      $result2=mysqli_query($conn,$query2);
      if($result1||$result2)  
      { 
      ?> 
         <script>
            alert("Data Updated"); 
            location.replace("Admin_Application_Search.php");
         </script> 
<?php        
         //header('location:Admin_Application_Search.php'); 
      }  
      else  
      { 
      ?> 
         <script>
            alert("Failed To Update");
         </script>
      <?php 
      } 
   }
?>
