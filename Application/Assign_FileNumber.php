<?php 
session_start();

$application_number= $_GET['apl_no'];// application number for retriving personal details

if(!isset($_SESSION['login_user']))
{
	echo "<script>alert('Session Expired');</script>";
	echo '<script type="text/javascript">
			location.replace("../index.php");
			</script>';
}

//Including database connection file
include "../connection.php" ; 

$query = " select * from ktfo_application where application_number='".$application_number."'";
$result = mysqli_query($conn,$query);
while($row=mysqli_fetch_assoc($result))
	{
  		$person_id = $row['person_id'];
		$file_no=$row['file_number'];
		
	
?>
<!DOCTYPE html>
<html>
<head>
    <title>Application_Update_File_Number</title>
    <link rel="stylesheet" href="../ktfo_css.css">
</head>
<body>
<!--Header-->
    <div class="header" align="center">
        <img class="site_logo" height="100" id="logo" src="../gvt.jpg" alt="Kerala logo" >
        <h1>KOTTAYAM TALUK FRONT OFFICE</h1>
    </div>
<!--navigation bar-->
    <div class="navbar">
        <a href="../Admin_Home.php">Home</a>
    </div>
    <br><br>
    <div class="form">
        <form action="" method="post">
            <table>
                <tr>
                    <td>Application Number</td>
                    <td><input name="application_number" type="text" disabled="disabled" value="<?php echo $application_number;?>"></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <?php 
	                $qry = "select * from ktfo_person where person_id='$person_id'";
                    $res = mysqli_query($conn,$qry);
 	                while($data=mysqli_fetch_assoc($res))
	                {
  		                $name = $data['name'];		
                        ?>
                            <td><input name="name" type="text" disabled="disabled" value="<?php echo $name; ?>"></td>
                        </tr>
                        <tr>
                            <td>File Number</td>
	                        <td><input name="file_number" type="text" value="<?php echo $file_no; }}?>"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><button name="update" type="submit" class="btn" onClick="return confirm('Are you sure about the updation?')">Update</button></td>
                </tr>
            </table>
        </div>
    </form>
    <br><br><br>
    <div style="position:fixed; bottom:0; width:100%;">
         <?php
         //including footer file
         include "../Footer.php";
         ?> 
   </div>
</body>
</html>
<?php
	if(isset($_POST['update']))
	{
		$file_no=$_POST['file_number'];		
		$sql="UPDATE ktfo_application set file_number='$file_no' where application_number=$application_number";
		if($conn->query($sql)== TRUE)
	 	{ 
	        $result=$conn->query("SELECT date_applied FROM ktfo_application WHERE application_number='$application_number';");
            $row=$result->fetch_assoc();
            $date=$row['date_applied'];
            ?>
            <script>
                alert("Updated Successfully");
                location.replace("Application_Update_FileNumber.php?date=<?php echo $date; ?>&search=search");
            </script> 
            <?php      
		} 
	else
		{
	?>
  	        <script>alert("Updation Failed");</script>  
<?php
		}
	}
?>